package com.maimieng;

public class Main {

    public static void main(String[] args) {
        System.out.println("testIntQueue");
        testIntQueue();

        System.out.println("testObjectQueue");
        testObjectQueue();
    }

    static void testIntQueue() {
        LoopQueue<Integer> q = new LoopQueue<Integer>(5);

        q.enQueue(2);
        q.enQueue(5);

        q.queueTraverse();

        int temp = q.deQueue();
        System.out.println(temp);

        q.queueTraverse();

        q.enQueue(7);
        q.enQueue(12);
        q.enQueue(19);
        q.enQueue(31);
        q.enQueue(50);

        q.queueTraverse();

        q.clearQueue();

        q.queueTraverse();
    }

    static void testObjectQueue() {
        LoopQueue<Student> q = new LoopQueue<Student>(5);

        q.enQueue(new Student(2, 2));
        q.enQueue(new Student(5, 5));

        q.queueTraverse();

        Student temp = q.deQueue();
        System.out.println(temp);

        q.queueTraverse();

        q.enQueue(new Student(7, 7));
        q.enQueue(new Student(12, 12));
        q.enQueue(new Student(19, 19));
        q.enQueue(new Student(31, 31));
        q.enQueue(new Student(50, 50));

        q.queueTraverse();
    }
}
