package com.maimieng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kingcos on 21/09/2016.
 */
public class LoopQueue<T> {

    private List<T> m_pQueue;
    private int m_iQueueLen;
    private int m_iCapacity;

    private int m_iHead;
    private int m_iTail;

    public LoopQueue(int m_iCapacity) {
        this.m_iCapacity = m_iCapacity;
        m_pQueue = new ArrayList<T>();
        clearQueue();
    }

    public void clearQueue() {
        m_iQueueLen = 0;

        m_iHead = 0;
        m_iTail = 0;
    }

    public boolean isEmpty() {
        return m_iQueueLen == 0;
    }

    public boolean isFull() {
        return m_iQueueLen == m_iCapacity;
    }

    public boolean enQueue(T element) {
        if (isFull()) {
            return false;
        }

        m_pQueue.add(element);
        m_iTail += 1;
        m_iTail %= m_iCapacity;
        m_iQueueLen += 1;
        return true;
    }

    public T deQueue() {
        if (isEmpty()) {
            return null;
        }
        T t = m_pQueue.remove(m_iHead);
        m_iHead += 1;
        m_iHead %= m_iCapacity;
        m_iQueueLen -= 1;
        return t;
    }

    public void queueTraverse() {
        for (int i = m_iHead; i < m_iHead + m_iQueueLen; i ++) {
            System.out.print(m_pQueue.get(i%m_iQueueLen) + " ");
        }
        System.out.println();
    }
}
