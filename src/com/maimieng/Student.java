package com.maimieng;

/**
 * Created by kingcos on 21/09/2016.
 */
public class Student {

    private int no;
    private int age;

    public Student(int no, int age) {
        this.no = no;
        this.age = age;
    }

    @Override
    public String toString() {
        String s = "[no:" + no + ", " + age + "]";
        return s;
    }
}
